require 'json'
nonogram = JSON.parse(File.read(ARGV[0]))
p nonogram
#Матрицы инструкций
Rows_inst = nonogram["nonogram"]["rows"]
Cols_inst = nonogram["nonogram"]["cols"]
#Создадим матрицы, отвечающие за решение каждого конкретного числа из инструкций
rows_status = Rows_inst.clone
rows_status.map!{ |x| Array.new(x.size, false) }
cols_status = Cols_inst.clone
cols_status.map!{ |x| Array.new(x.size, false) }
#Матрица решения
sol = Array.new(Rows_inst.size).map!{Array.new(Cols_inst.size)}
def test(line_1, line_2)
  case line_1
  when nil
    case line_2
      when nil
        nil
      when true
        true
      when false
        false
      end
  when false
    case line_2
      when nil
      false
      when true
      true
      when false
      false
    end
  when true
    case line_2
      when nil
        true
      when true
          true
      when false
        true
    end
  end
end

#Вывод решения японского кроссворда
def print_sol(n)
  m = n.clone
  #Преобразование бинарной логики в псевдографику
  (0...Rows_inst.size).each do |i|
    (0...Cols_inst.size).each do |j|
      if (n[i][j] == true)
        m[i][j] = "\u2592"
      else
        if (n[i][j] == false)
          m[i][j] = 'x'
        else
          m[i][j] = '0'
        end
      end
    end
  end
  #Вывод решения
  (0...Rows_inst.size).each do |i|
    (0...Cols_inst.size).each do |j|
      print(m[i][j])
    end
    puts ''
  end
  puts '<end>'
end
#Пересечение линий
def cross(line_1,line_2)
  temp_line = Array.new
  (0...line_1.size).each do |i|
    if (line_1[i] & line_2[i] == true)
      temp_line[i] = true
    else
      temp_line[i] = nil
    end
  end
  temp_line
end
#Сложение линий
def add(line_1, line_2)
  temp_line = Array.new
  (0...line_1.size).each do |i|
    #Поменять, тк операция не коммутативна
    temp_line[i] = test(line_1[i], line_2[i])
    #temp_line[i] = line_1[i] | line_2[i]
  end
  temp_line
end
#Проверка и заполнение тривиальных строчек
def trivial_line(line, line_length)
  if line.sum + line.size - 1 == line_length
    line_triv = Array.new
    status_temp = Array.new
    (0...line.size).each do |i|
      (0...line[i]).each do |k|
        line_triv << true
      end
      line_triv << false
      status_temp << true
    end
    line_triv.delete_at(line_triv.size - 1)
  else
    line_triv = Array.new(line_length, nil)
  end
  [line_triv, status_temp]
end
(0...Rows_inst.size).each do |i|
  line_temp, status_temp = trivial_line(Rows_inst[i], Cols_inst.size)
  sol[i] = add(line_temp, sol[i])
  rows_status[i] = status_temp || rows_status[i]
end
(0...Cols_inst.size).each do |i|
  line_temp, status_temp = trivial_line(Cols_inst[i], Rows_inst.size)
  sol_temp = sol.transpose
  sol_temp[i] = add(line_temp, sol_temp[i])
  cols_status[i] = status_temp || rows_status[i]
  sol = sol_temp.transpose
end
#Выставление "Средних символов"
(0...Rows_inst.size).each do |i|
  left = 0
  right = Cols_inst.size
  (0...Rows_inst[i].size).each do |j|
    left = left + Rows_inst[i][j] + 1
    right = right - Rows_inst[i][j] - 1
    if (Rows_inst[i][j] > ((Cols_inst.size - (Rows_inst[i].sum - Rows_inst[i][j]) - (Rows_inst[i].size - 1))%2))
      left = left - 1
      right = right + 1
      temp_left = Array.new(Cols_inst.size, false)
      temp_right = Array.new(Cols_inst.size, true)
      (0...left).each do |l|
        temp_left[l] = true
      end
      (0...right).each do |r|
        temp_right[r] = false
      end
      row_temp = Array.new(Cols_inst.size)
        row_temp = cross(temp_left, temp_right)
      (0...Cols_inst.size).each do |k|
        # if (row_temp[k] == false)
        #   row_temp[k] = nil
        # end
        sol[i][k] = test(sol[i][k], row_temp[k])
      end
      left = 0
      right = 0
    end
  end
end

(0...Cols_inst.size).each do |i|
  left = 0
  right = Rows_inst.size
  (0...Cols_inst[i].size).each do |j|
    left = left + Cols_inst[i][j] + 1
    right = right - Cols_inst[i][j] - 1
    if (Cols_inst[i][j] > ((Rows_inst.size - (Cols_inst[i].sum - Cols_inst[i][j]) - (Cols_inst[i].size - 1))%2))
      left = left - 1
      right = right + 1
      temp_left = Array.new(Rows_inst.size, false)
      temp_right = Array.new(Rows_inst.size, true)
      (0...left).each do |l|
        temp_left[l] = true
      end
      (0...right).each do |r|
        temp_right[r] = false
      end
      row_temp = Array.new(Rows_inst.size)
      row_temp = cross(temp_left, temp_right)
      (0...Rows_inst.size).each do |k|
        # if (row_temp[k] == false)
        #   row_temp[k] = nil
        # end
        #p row_temp
        sol_temp = sol.transpose
        sol_temp[i][k] = test(sol_temp[i][k], row_temp[k])
        sol = sol_temp.transpose
      end
      left = 0
      right = 0
    end
  end
end
print_sol(sol)
